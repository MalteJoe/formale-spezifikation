#define NUM_PHIL 4

int forks[NUM_PHIL] = -1

proctype phil (int id) {
int right = id; int left = (id + 1) % NUM_PHIL;
do
  ::
  printf ("thinking...\n");
  if :: skip;
      atomic{ forks[left]  == -1 -> forks[left]  = id };
      atomic{ forks[right] == -1 -> forks[right] = id };
     :: skip;
      atomic{ forks[right] == -1 -> forks[right] = id };
      atomic{ forks[left]  == -1 -> forks[left]  = id };
  fi;

  printf ("eating...\n");
  forks[left] = -1;
  forks[right] = -1;
od
}

init {
int i = 0;
do
:: i >= NUM_PHIL -> break
:: else -> run phil(i);
           i ++
od
}
